package com.kgc.cn.vo;

import com.google.common.collect.Sets;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@ApiModel(value = "主页展示用房屋信息model")
public class HouseMainVo implements Serializable {
    private String id;
    @ApiModelProperty(value = "合租类型，整租：1 合租：2")
    private Integer type;
    @ApiModelProperty(value = "区域")
    private String area;
    @ApiModelProperty(value = "小区名")
    private String community;
    @ApiModelProperty(value = "卧室类型 不存在：-1 主卧：0 次卧：1 床位：2")
    private Integer roomtype;
    @ApiModelProperty(value = "户型（厅室）")
    private String housetype;
    @ApiModelProperty(value = "楼层")
    private String floor;
    @ApiModelProperty(value = "建筑面积")
    private Double acreage;
    @ApiModelProperty(value = "租金")
    private Integer money;
    @ApiModelProperty(value = "地址")
    private String address;
    @ApiModelProperty(value = "更新时间")
    private String uptime;
    @ApiModelProperty("短租 不支持：0 一个月：1 两个月：2 三个月：3 四个月：4 五个月：5 六个月：6")
    private Integer shortrent;
    @ApiModelProperty(value = "装修情况 无装修：0 普通装修：1 精装修：2 豪华装修：3")
    private Integer fitment;
    @ApiModelProperty(value = "出租要求")
    private List<String> demandList;
    @ApiModelProperty(value = "房源亮点")
    private List<String> characteristicList;
    @ApiModelProperty(value = "标签")
    private Set<String> tagList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public Integer getRoomtype() {
        return roomtype;
    }

    public void setRoomtype(Integer roomtype) {
        this.roomtype = roomtype;
    }

    public String getHousetype() {
        return housetype;
    }

    public void setHousetype(String housetype) {
        this.housetype = housetype;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        if (floor <= 6) {
            this.floor = "低层";
        } else if (floor <= 12) {
            this.floor = "中层";
        } else {
            this.floor = "高层";
        }
    }

    public Double getAcreage() {
        return acreage;
    }

    public void setAcreage(Double acreage) {
        this.acreage = acreage;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUptime() {
        return uptime;
    }

    public void setUptime(Date uptime) {
        long currentTime = System.currentTimeMillis();
        long updateTime = uptime.getTime();
        long intervalTime = (currentTime - updateTime) / 1000;
        if (intervalTime < 60) {
            this.uptime = intervalTime + "秒前";
        } else if (intervalTime < 60 * 60) {
            this.uptime = intervalTime / 60 + "分钟前";
        } else if (intervalTime < 60 * 60 * 24) {
            this.uptime = intervalTime / (60 * 60) + "小时前";
        } else if (intervalTime < 60 * 60 * 24 * 7) {
            this.uptime = intervalTime / (60 * 60 * 24) + "天前";
        } else {
            this.uptime = null;
        }
    }

    public Integer getShortrent() {
        return shortrent;
    }

    public void setShortrent(Integer shortrent) {
        this.shortrent = shortrent;
    }

    public Integer getFitment() {
        return fitment;
    }

    public void setFitment(Integer fitment) {
        this.fitment = fitment;
    }

    public List<String> getDemandList() {
        return demandList;
    }

    public void setDemandList(List<String> demandList) {
        this.demandList = demandList;
    }

    public List<String> getCharacteristicList() {
        return characteristicList;
    }

    public void setCharacteristicList(List<String> characteristicList) {
        this.characteristicList = characteristicList;
    }

    public Set<String> getTagList() {
        return tagList;
    }

    public void setTagList() {
        Set<String> tag = Sets.newHashSet();
        switch (shortrent) {
            case 1:
                tag.add("短租1个月");
                break;
            case 2:
                tag.add("短租2个月");
                break;
            case 3:
                tag.add("短租3个月");
                break;
            case 4:
                tag.add("短租4个月");
                break;
            case 5:
                tag.add("短租5个月");
                break;
            case 6:
                tag.add("短租6个月");
                break;
        }
        switch (fitment) {
            case 0:
                tag.add("无装修");
                break;
            case 1:
                tag.add("普通装修");
                break;
            case 2:
                tag.add("精装修");
                break;
            case 3:
                tag.add("豪华装修");
                break;
        }
        demandList.forEach(demand -> {
            if ("只限女生".equals(demand)) {
                tag.add("只限女生");
            }
        });
        if (tag.size() == 2) {
            if (!CollectionUtils.isEmpty(characteristicList)) {
                tag.add(characteristicList.get(0));
            }
        } else if (tag.size() <= 1) {
            if (characteristicList.size() >= 3) {
                tag.add(characteristicList.get(0));
            } else {
                tag.addAll(characteristicList);
            }
        }
        this.tagList = tag;
    }
}
