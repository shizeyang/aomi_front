package com.kgc.cn.service;

import com.kgc.cn.utils.result.ReturnResult;
import com.kgc.cn.vo.JointRentVo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;


/**
 * @Date 2020/1/9 10:03
 * @Creat by Crane
 */
@FeignClient(name = "rent-service")
public interface JointRentService {

    @PostMapping(value = "/jointRent/add")
    ReturnResult add(@RequestBody JointRentVo jointRentVo);

    @GetMapping(value = "/jointRent/show")
    ReturnResult show();


}
