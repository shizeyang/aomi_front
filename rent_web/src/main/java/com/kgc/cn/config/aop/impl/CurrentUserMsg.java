package com.kgc.cn.config.aop.impl;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.User;
import com.kgc.cn.config.aop.CurrentUser;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Objects;

public class CurrentUserMsg implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(User.class) &&
                parameter.hasParameterAnnotation(CurrentUser.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String userJsonStr = (String) webRequest.getAttribute("userJsonString", RequestAttributes.SCOPE_REQUEST);
        User user = JSONObject.parseObject(userJsonStr, User.class);
        if (!Objects.isNull(user)) {
            return user;
        }
        return null;
    }
}
