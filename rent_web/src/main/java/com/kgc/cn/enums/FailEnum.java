package com.kgc.cn.enums;

/**
 * @Date 2019/11/30 8:31
 * @Creat by admin
 */
public enum FailEnum {
    HAS_BEAN_USED(001, "该房屋信息已经被发布"),
    PAGE_LOST(002,"页面走丢了");


    private int code;
    private String msg;

    FailEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
