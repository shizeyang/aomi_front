package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Date 2020/1/8 9:30
 * @Creat by Crane
 */
@Data
@ApiModel(value = "合租")
public class JointRentVo implements Serializable {

    @ApiModelProperty("用户id")
    private String uid;

    @ApiModelProperty("区域")
    private String area;

    @ApiModelProperty("小区名")
    private String community;

    @ApiModelProperty("面积")
    private double acreage;

    @ApiModelProperty("户型")
    private String houseType;

    @ApiModelProperty("月租金")
    private int money;

    @ApiModelProperty("详细地址")
    private String address;

    @ApiModelProperty("短租  不支持：0 一个月：1 两个月：2 三个月：3 四个月：4 五个月：5 六个月：6")
    private int shortRent;

    @ApiModelProperty("装修情况 无装修：0 普通装修：1 精装修：2 豪华装修：3")
    private int fitment;

    @ApiModelProperty("卧室朝向 东：0 南：1 西：2 北：3")
    private int roomDirection;

    @ApiModelProperty("出租要求")
    private List<Integer> demand;

    @ApiModelProperty("房源特点")
    private List<Integer> characteristic;

    @ApiModelProperty("描述")
    private String describe;

    @ApiModelProperty("卧室类型 主卧：0 次卧：1 床位：2")
    private int roomType;

    @ApiModelProperty("起租时间")
    private Date startRent;

    @ApiModelProperty("看房时间")
    private Date watchTime;

    @ApiModelProperty("宜居人数")
    private int count;

    @ApiModelProperty("联系人姓名")
    private String linkMan;

    @ApiModelProperty("身份 个人房东：0 经纪人：1")
    private int status;

    @ApiModelProperty("联系人手机号")
    private String phone;

    @ApiModelProperty("房屋配置")
    private List<Integer> configuration;

    @ApiModelProperty("楼层")
    private int floor;
}