package com.kgc.cn.vo;

import com.kgc.cn.model.Characteristic;
import com.kgc.cn.model.Configuration;
import com.kgc.cn.model.Demand;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Map;

@ApiModel
public class LeaseDetailsVo {
    @ApiModelProperty("出租要求")
    private Map<String,List<Demand>> demands;
    @ApiModelProperty("房屋描述")
    private Map<String,List<Configuration>> configurations;
    @ApiModelProperty("房屋特点")
    private Map<String,List<Characteristic>> characteristics;

    public LeaseDetailsVo() {
    }

    public LeaseDetailsVo(Map<String, List<Demand>> demands, Map<String, List<Configuration>> configurations, Map<String, List<Characteristic>> characteristics) {
        this.demands = demands;
        this.configurations = configurations;
        this.characteristics = characteristics;
    }

    public Map<String, List<Demand>> getDemands() {
        return demands;
    }

    public void setDemands(Map<String, List<Demand>> demands) {
        this.demands = demands;
    }

    public Map<String, List<Configuration>> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(Map<String, List<Configuration>> configurations) {
        this.configurations = configurations;
    }

    public Map<String, List<Characteristic>> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(Map<String, List<Characteristic>> characteristics) {
        this.characteristics = characteristics;
    }
}
