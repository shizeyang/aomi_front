package com.kgc.cn.service.impl;

import com.kgc.cn.mapper.HouseMapper;
import com.kgc.cn.model.House;
import com.kgc.cn.model.HouseExample;
import com.kgc.cn.model.Houses;
import com.kgc.cn.service.HouseDetailService;
import com.kgc.cn.vo.HouseDetailQueryVo;
import com.kgc.cn.vo.HouseQueryVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class HouseDetailServiceImpl implements HouseDetailService {
    @Resource
    private HouseMapper houseMapper;

    @Override
    public HouseDetailQueryVo queryDetail(String id) {
        houseMapper.addPageView(id);
        Houses houses = houseMapper.queryDetailById(id);
        HouseDetailQueryVo houseDetailQueryVo = new HouseDetailQueryVo();
        BeanUtils.copyProperties(houses, houseDetailQueryVo);
        if (houses != null) {
            return houseDetailQueryVo;
        }
        return null;
    }



}
