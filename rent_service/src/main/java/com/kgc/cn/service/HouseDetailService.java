package com.kgc.cn.service;


import com.kgc.cn.vo.HouseDetailQueryVo;


public interface HouseDetailService {
    /**
     * 展示详情
     */
    HouseDetailQueryVo queryDetail(String id);



}
