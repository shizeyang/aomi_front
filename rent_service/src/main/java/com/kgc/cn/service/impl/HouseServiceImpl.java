package com.kgc.cn.service.impl;

import com.kgc.cn.mapper.HouseMapper;
import com.kgc.cn.mapper.RotationMapper;
import com.kgc.cn.model.Houses;
import com.kgc.cn.model.RotationExample;
import com.kgc.cn.service.HouseService;
import com.kgc.cn.vo.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HouseServiceImpl implements HouseService {

    @Resource
    private HouseMapper houseMapper;

    @Resource
    private RotationMapper rotationMapper;

    @Override
    public PageInfo queryHouseWithPage(int pageNo, int pageSize, HouseQueryVo houseQueryVo) {
        houseQueryVo.setSortWay();
        PageInfo pageInfo = new PageInfo();
        int totalNum = houseMapper.queryHouseByPage(null, null, houseQueryVo).size();
        pageInfo.setPage(totalNum, pageSize, pageNo);
        List<Houses> housesList = houseMapper.queryHouseByPage(pageNo * pageSize - pageSize, pageSize, houseQueryVo);
        List<HouseMainVo> houseList = houseMapper.queryHouseByPage(pageNo * pageSize - pageSize, pageSize, houseQueryVo)
                .stream().map(house -> {
                    HouseMainVo houseMainVo = house.toHouseMainVo();
                    houseMainVo.setTagList();
                    return houseMainVo;
                }).collect(Collectors.toList()).stream().filter(houses -> {
                    if (StringUtils.isNotEmpty(houseQueryVo.getDemand())) {
                        if (!houses.getDemandList().contains(houseQueryVo.getDemand())) {
                            return false;
                        }
                    }
                    if (StringUtils.isNotEmpty(houseQueryVo.getCharacteristic())) {
                        if (!houses.getCharacteristicList().contains(houseQueryVo.getCharacteristic())) {
                            return false;
                        }
                    }
                    return true;
                }).collect(Collectors.toList());
        pageInfo.setList(houseList);
        return pageInfo;
    }

    @Override
    public List<RotationQueryVo> queryRotationChart() {
        RotationExample rotationExample = new RotationExample();
        List<RotationQueryVo> rotationList = rotationMapper.selectByExample(rotationExample)
                .stream().map(rotation -> {
                    RotationQueryVo rotationQueryVo = new RotationQueryVo();
                    BeanUtils.copyProperties(rotation, rotationQueryVo);
                    return rotationQueryVo;
                }).collect(Collectors.toList());
        return rotationList;
    }

    @Override
    public List<String> queryArea() {
        return houseMapper.queryArea();
    }

    @Override
    public List<MoneyScope> queryMoney() {
        return houseMapper.queryMoneyScope();
    }

    @Override
    public Apartment queryApartment() {
        Apartment apartment = new Apartment();
        apartment.setTypeList(houseMapper.queryType());
        apartment.setHouseTypeList(houseMapper.queryhousetype());
        return apartment;
    }

    @Override
    public Screen queryScreen() {
        Screen screen = new Screen();
        screen.setCharacteristicList(houseMapper.querycharacteristic());
        screen.setDemandList(houseMapper.querydemand());
        screen.setDirectionList(houseMapper.querydirection());
        return screen;
    }
}
