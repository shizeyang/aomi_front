package com.kgc.cn.enums;

import lombok.Getter;

public interface Enums {

    int getCode();

    String getMsg();

    @Getter
    enum CommonEnum implements Enums {
        LEASE_ADD_FAIL(6001,"房屋发布失败"),
        LEASE_ALEARY_FAIL(6002,"房屋以被发布，请核对房屋信息"),
        LOGIN_ERROR(10000, "账号或密码错误，登录失败"),
        UPDATE_ERROR(10009, "删除失败"),
        HOUSE_FAIL(18888, "该房屋已下架");;
        private int code;
        private String msg;

        public static final String LEASE_ADD_SUCCESS = "房屋发布成功";
        public static final String CHARACTERISTIC = "房屋特点";
        public static final String CONFIGURATIONN = "房屋描述";
        public static final String DEMAND = "出租要求";
        public static final String PICTURE_ADD_SUCCESS = "上传图片成功";

        public static final String DEL_SUCCESS = "删除成功";
        public static final String DELL_SUCCESS = "举报成功";

        CommonEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
