package com.kgc.cn.utils.id;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @Date 2020/1/8 10:59
 * @Creat by Crane
 */
public class QueryHouseId {
    public static String queryId(){
        String uid = UUID.randomUUID().toString().replace("-","").substring(10);
        String time = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String hid = time + uid;
        return hid;
    }
}
