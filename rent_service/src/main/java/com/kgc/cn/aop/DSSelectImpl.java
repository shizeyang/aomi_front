package com.kgc.cn.aop;

import com.kgc.cn.config.MultipleDataSourceHelper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Method;

@Component
@Aspect
public class DSSelectImpl {
     @Before("com.kgc.cn.aop.DSPointcut.selectorPointcut()")
    public void changeDS(JoinPoint joinpoint){
         MethodSignature methodSignature = (MethodSignature) joinpoint.getSignature();
         Method method = methodSignature.getMethod();
         DSSelect dsSelect = method.getAnnotation(DSSelect.class);
         if (ObjectUtils.isEmpty(dsSelect)) return;

         MultipleDataSourceHelper.set(dsSelect.value());
     }
}
